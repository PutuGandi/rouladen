#!/usr/bin/env python3

import yaml
import jinja2
import sys
import os

templateLoader = jinja2.FileSystemLoader(searchpath="./src/chart/")
templateEnv = jinja2.Environment(loader=templateLoader)
templateFile = "values.yaml.j2"
template = templateEnv.get_template(templateFile)
resourceManifestContent = template.render(CI_COMMIT_SHORT_SHA=os.environ['CI_COMMIT_SHORT_SHA'])
f = open("./dist/chart/values.yaml","w")
f.write(resourceManifestContent)
f.close()
